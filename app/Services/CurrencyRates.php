<?php

namespace App\Services;

use App\Models\Currency;
use Exception;
use GuzzleHttp\Client;

class CurrencyRates
{
    public static function getRates()
    {
        $baseCurrency = CurrencyConversion::getBaseCurrency();

        $url = config('currency_rates.api_url');
        $key = config('currency_rates.api_key');

        $client = new Client();
        $response = $client->request('GET', $url . '?access_key=' . $key);

        if ($response->getStatusCode() !== 200) {
            throw new Exception('There is a problem with currency rate service');
        }

        $rates = json_decode($response->getBody()->getContents(), true)['rates'];
        $rates[$baseCurrency->code] = ($rates[$baseCurrency->code])/($rates['RUB']);

        foreach (CurrencyConversion::getCurrencies() as $currency) {
            if (!$currency->isMain()) {
                if (!isset($rates[$currency->code])) {
                    throw new Exception('There is a problem with currency ' . $currency->code);
                } else {
                    $baseCurrency->update(['rate' => $rates[$baseCurrency->code]]);
                    $baseCurrency->touch();
                }
            }
        }
    }
}

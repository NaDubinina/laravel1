<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductsFilterRequest;
use App\Http\Requests\SubscriptionRequest;
use App\Models\Category;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class MainController extends Controller
{
    public function index(ProductsFilterRequest $request)
    {
//        Product::query()->insert(['name' => 'телефон1', 'code' => 'code1', 'price' => '100' , 'description' => 'uuuu', 'category_id' => 1]);
//        Product::query()->insert(['name' => 'телефон2', 'code' => 'code2', 'price' => '100' , 'description' => 'uuuu', 'category_id' => 2]);
//        Product::query()->insert(['name' => 'телефон3', 'code' => 'code3', 'price' => '100' , 'description' => 'uuuu', 'category_id' => 3]);
        $productsQuery = Product::query();

        if ($request->filled('price_from')) {
            $productsQuery->where('price', '>=', $request->price_from);
        }

        if ($request->filled('price_to')) {
            $productsQuery->where('price', '<=', $request->price_to);
        }

        foreach (['hit', 'new', 'recommend'] as $field) {
            if ($request->has($field)) {
                $productsQuery->$field();
            }
        }

        $products = $productsQuery->paginate(3)->withPath("?" . $request->getQueryString());
        return view('index', compact('products'));
    }

    public function product($category, $productCode)
    {
        $product = Product::withTrashed()->byCode($productCode)->firstOrFail();
        return view('product', compact('product'));
    }

    public function categories()
    {
//        Category::query()->insert(['name' => 'Мобильные телефоны', 'code' => 'mobiles' , 'description' => 'uuuu']);
//        Category::query()->insert(['name' => 'Портативная техника', 'code' => 'portable' , 'description' => 'uuuu']);
//        Category::query()->insert(['name' => 'Бытовая техника', 'code' => 'appliances' , 'description' => 'uuuu']);

        $categories = Category::query()->paginate(1);
        return view('categories', ['categories' => $categories]);
    }

    public function category($categoryCode = 'code1')
    {
        $category = Category::query()->where('code', $categoryCode)->first();
        return view('category', compact('category'));
    }

    public function subscribe(SubscriptionRequest $request, Product $product)
    {
        Subscription::create([
            'email' => $request->email,
            'product_id' => $product->id,
        ]);

        return redirect()->back()->with('success', 'Спасибо, мы сообщим вам о поступлении товара');
    }
    public function changeLocale($locale)
    {
        $availableLocales = ['ru', 'en'];
        if (!in_array($locale, $availableLocales)) {
            $locale = config('app.locale');
        }

        session(['locale' => $locale]);
        App::setLocale($locale);
        return redirect()->back();
    }

    public function changeCurrency($currencyCode)
    {
        $currency = Currency::byCode($currencyCode)->firstOrFail();
        session(['currency' => $currency->code]);
        return redirect()->back();
    }
}


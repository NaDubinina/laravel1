<?php

return [
    'online_shop' => 'Online Shop',
    'all_products' => 'All products',
    'product' => 'Product',
    'categories' => 'Categories',
    'cart' => 'Your cart',
    'reset_project' => 'Reset project to default state',
    'current_lang' => 'en',
    'set_lang' => 'ru',
    'login' => 'Login',
    'logout' => 'Logout',
    'admin_panel' => 'Admin panel',
    'project_reset' => 'The Project was reset',
];

<?php

return [
    'online_shop' => 'Интернет Магазин',
    'all_products' => 'Все товары',
    'product' => 'Товар',
    'categories' => 'Категории',
    'cart' => 'В корзину',
    'reset_project' => 'Сбросить проект в начальное состояние',
    'login' => 'Войти',
    'logout' => 'Выйти',
    'admin_panel' => 'Панель администратора',
    'title' => 'Главная',
    'current_lang' => 'ru',
    'set_lang' => 'en',
];

@extends('layouts.master')

@section('title', 'Категория ')
@section('content')
    <div class="container">
        <div class="starter-template">
            <h1>
                {{ $category->name }}
            </h1>
            <p>
                {{ $category->description }}        </p>
            @foreach($category->products as $product)
                @include('layouts.card', compact($product, $category))
            @endforeach
        </div>
    </div>
@endsection
